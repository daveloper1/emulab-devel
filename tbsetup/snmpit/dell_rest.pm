#!/usr/bin/perl -w

#
# Copyright (c) 2019-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LGPL
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation; either version 2.1 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
# 
# You should have received a copy of the GNU Lesser General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#

#
# Module for Dell OS10 Enterprise RESTCONF API.
# XXX taken from FreeNAS REST API support and probably very similar to other
# REST APIs...
#
# Some of the spec generated here are from trial and error. The rest came
# later and are from turning on "cli mode rest-translate" on an OS10 switch
# and doing the corresponding CLI command to generate a curl command.
#

package dell_rest;
use strict;

use English;
use HTTP::Tiny;
use JSON::PP;
use MIME::Base64;
use Data::Dumper;
use Socket;
use Time::HiRes qw(gettimeofday);
use libtestbed;

$| = 1; # Turn off line buffering on output

sub new($$$$)
{
    # The next two lines are some voodoo taken from perltoot(1)
    my $proto = shift;
    my $class = ref($proto) || $proto;

    my $name = shift;
    my $debugLevel = shift;
    my $userpass = shift;  # username and password

    #
    # Create the actual object
    #
    my $self = {};

    #
    # Set the defaults for this object
    # 
    if (defined($debugLevel)) {
        $self->{DEBUG} = $debugLevel;
    } else {
        $self->{DEBUG} = 0;
    }

    $self->{NAME} = $name;
    ($self->{USERNAME}, $self->{PASSWORD}) = split(/:/, $userpass);
    if (!$self->{USERNAME} || !$self->{PASSWORD}) {
	warn "dell_rest: ERROR: must pass in username AND password!\n";
	return undef;
    }

    if ($self->{DEBUG}) {
        print "dell_rest initializing for $self->{NAME}, " .
            "debug level $self->{DEBUG}\n" ;
    }

    # Make it a class object
    bless($self, $class);

    return $self;
}

#
# Make a request via the RESTCONF API.
#   $method is "GET", "PUT", "POST", or "DELETE"
#   $path is the resource path, e.g., "interfaces/ethernet"
#   $datap is a reference to a hash of KEY=VALUE input content (default is ())
#   $exstat is the expected success status code if not the method default
#   $errorp is a reference to a string, used to return error string if !undef
# Return value is the decoded (as a hash) JSON KEY=VALUE returned by request
# Returns undef on failure.
#
sub call($$$;$$$$)
{
    my ($self,$method,$path,$datap,$exstat,$errorp,$raw) = @_;
    my %data = $datap ? %$datap : ();
    my ($datastr,$paramstr);
    my %status = (
	"GET"    => 200,
	"PUT"    => 201,
	"POST"   => 201,
	"DELETE" => 204,
	"PATCH"  => 204
    );
    my %status2 = (
	"PUT"    => 204
    );

    my $auth = $self->{USERNAME} . ":" . $self->{PASSWORD};
    my $server = $self->{NAME};
    if (keys %data > 0) {
	$datastr = encode_json(\%data);
    } else {
	$datastr = "";
    }

    my $url = "https://$server/restconf/data/$path";
    # we want to know with basic debugging whenever we go to the switch
    print STDERR "dell_rest: make RESTAPI ('$path') $method call to $server\n"
	if ($self->{DEBUG});
    print STDERR "$server: REQUEST: method=$method URL=$url\nCONTENT=$datastr\n"
	if ($self->{DEBUG} > 3);

    my %headers = (
	"Accept"        => "application/json",
	"Authorization" => "Basic " . MIME::Base64::encode_base64($auth, "")
    );
    if ($method eq "POST" || $method eq "PATCH" || $method eq "PUT") {
	$headers{"Content-Type"} = "application/json";
    }

    my $http = $self->{HTTP};
    if (!$http) {
	$http = $self->{HTTP} = HTTP::Tiny->new("timeout" => 10);
	if ($http) {
	    print STDERR "$server: established new HTTP connection\n"
		if ($self->{DEBUG} > 1);
	} else {
	    print STDERR "$server: could not open HTTP connection!\n";
	    return undef;
	}
    } else {
	print STDERR "$server: using existing HTTP connection\n"
	    if ($self->{DEBUG} > 1);
    }
    my %options = ("headers" => \%headers, "content" => $datastr); 

    my $stamp = gettimeofday()
	if ($self->{DEBUG} > 1);

    # Serialize calls, see lock() comment.
    if ($self->lock()) {
	if ($self->{DEBUG} > 2) {
	    my $st = sprintf "%.3f", gettimeofday() - $stamp;
	    print STDERR "$server: REQUEST: could not acquire lock after ${st} sec.\n";
	}
	my $msg = "Switch too busy!";
	if ($errorp) {
	    $$errorp = $msg;
	} else {
	    warn("*** ERROR: dell_rest: $msg");
	}
	return undef;
    }
    if ($self->{DEBUG} > 2) {
	my $st = sprintf "%.3f", gettimeofday() - $stamp;
	print STDERR "$server: REQUEST: got lock after ${st} sec.\n";
    }
    my $res = $http->request($method, $url, \%options);
    $self->unlock();
    if ($self->{DEBUG} > 1) {
	$stamp = sprintf "%.3f", gettimeofday() - $stamp;
	print STDERR "$server: RESTAPI ('$path') call done in ${stamp} sec.\n";
	print STDERR "$server: RESPONSE: ", Dumper($res), "\n"
	    if ($self->{DEBUG} > 3);
    }
    $exstat = $status{$method}
	if (!defined($exstat));
    my $exstat2 = exists($status2{$method}) ? $status2{$method} : $exstat;

    if ($res->{'success'} &&
	($res->{'status'} == $exstat || $res->{'status'} == $exstat2)) {
	if (exists($res->{'headers'}{'content-type'}) &&
	    ($res->{'headers'}{'content-type'} eq "application/json" ||
	     $res->{'headers'}{'content-type'} eq "application/yang-data+json")) {
	    return $raw ?
		$res->{'content'} : JSON::PP->new->decode($res->{'content'});
	}
	if (!exists($res->{'content'})) {
	    return {};
	}
	if (!ref($res->{'content'})) {
	    return { "content" => $res->{'content'} };
	}
	my $msg = "Unparsable content: " . Dumper($res->{'content'});
	if ($errorp) {
	    $$errorp = $msg;
	} else {
	    warn("*** ERROR: dell_rest: $msg");
	}
	return undef;
    }
    if ($res->{'reason'}) {
	my $content;

	if (exists($res->{'content'}) &&
	    exists($res->{'headers'}{'content-type'})) {
	    my $ctype = $res->{'headers'}{'content-type'};
	    if ($ctype eq "text/plain") {
		$content = $res->{'content'};
	    } elsif ($ctype eq "application/json" ||
		     $ctype eq "application/yang-data+json") {
		my $cref =
		    JSON::PP->new->decode($res->{'content'});
		if ($cref && ref $cref) {
		    if (exists($cref->{'ietf-restconf:errors'}) &&
			exists($cref->{'ietf-restconf:errors'}->{'error'})) {
			$content = $cref->{'ietf-restconf:errors'}->{'error'};
			$content = @{$content}[0]->{'error-message'};
		    }
		} elsif ($cref) {
		    $content = $cref;
		} else {
		    $content = $res->{'content'};
		}
	    }
	}
	my $msg = "Request failed: " . $res->{'reason'};
	if ($content) {
	    $msg .= "\nRESTCONF error: $content";
	}
	if ($errorp) {
	    $$errorp = $msg;
	} else {
	    warn("*** ERROR: dell_rest: $msg");
	}
	return undef;
    }

    my $msg = "Request failed: " . Dumper($res);
    if ($errorp) {
	$$errorp = $msg;
    } else {
	warn("*** ERROR: dell_rest: $msg");
    }
    return undef;
}

#
# Create a perl hash (suitable for JSON encoding) representing a new VLAN.
#
sub makeVlanSpec($$$)
{
    my ($self,$tag,$name) = @_;

    my $vname = "vlan$tag";
    my $vlanhash = {
	"interface" => [{
	    "type" => "iana-if-type:l2vlan",
	    "enabled" => JSON::PP::true,
	    "description" => "$name",
	    "name" => "$vname"
	}]
    };

    return $vlanhash;
}

#
# Make sure each port appears only once in the given list.
# Returns a new list.
#
# XXX without this, the REST data/interfaces/interface/vlanN PATCH command
# (for adding ports to a VLAN) will fail with "Conflict" and "entry exists".
#
sub uniqueList(@) {
    my (@olist) = @_;

    my %pseen = ();
    my @nlist = ();
    foreach my $p (@olist) {
	if (!exists($pseen{$p})) {
	    push @nlist, $p;
	    $pseen{$p} = 1;
	}
    }
    return @nlist;
}

sub addPortsVlanSpec($$$$)
{
    my ($self,$tag,$uportref,$tportref) = @_;

    my $vname = "vlan$tag";
    my @uports = ($uportref ? @{$uportref} : ());
    my @tports = ($tportref ? @{$tportref} : ());
    my $vlanhash = {
	"interface" => [{
	    "name" => "$vname",
	}]
    };

    if (@uports) {
	$vlanhash->{"interface"}->[0]->{"dell-interface:untagged-ports"} =
	    [uniqueList(@uports)];
    }
    if (@tports) {
	$vlanhash->{"interface"}->[0]->{"dell-interface:tagged-ports"} =
	    [uniqueList(@tports)];
    }
    
    return $vlanhash;
}

sub removeTaggedPortsVlanSpec($$$)
{
    my ($self,$tag,$tportlist) = @_;

    my @ports = uniqueList(@{$tportlist});
    my $vlanhash = {
	"ietf-interfaces:interfaces" => {
	    "dell-interface-range:interface-range" => [{
		"type" => "iana-if-type:l2vlan",
		"name" => "$tag",
		"config-template" => {
		    "dell-interface:tagged-ports" => \@ports,
		    "delete-object" => [ "tagged-ports" ]
		}
	    }]
	}
    };

    return $vlanhash;
}

sub removeVlansSpec($$)
{
    my ($self,@taglist) = @_;

    my $tagstr = join(',', uniqueList(@taglist));
    my $vlanhash = {
	"ietf-interfaces:interfaces" => {
	    "dell-interface-range:interface-range" => [{
		"type" => "iana-if-type:l2vlan",
		"name" => $tagstr,
		"operation" => "DELETE",
	    }]
	}
    };

    return $vlanhash;
}

sub trunkPortSpec($$)
{
    my ($self,$iface) = @_;

    my $porthash = {
	"interface" => [{
	    "name" => "$iface",
	    "dell-interface:mode" => "MODE_L2HYBRID"
	}]
    };

    return $porthash;
}

sub enablePortSpec($$$)
{
    my ($self,$turnon,$iface) = @_;
    my $state = $turnon ? JSON::PP::true : JSON::PP::false;

    my $porthash = {
	"interface" => [{
	    "name" => "$iface",
	    "enabled" => $state
	}]
    };

    return $porthash;
}

sub enableMultiplePortsSpec($$@)
{
    my ($self,$turnon,@ifaces) = @_;
    my $state = $turnon ? JSON::PP::true : JSON::PP::false;

    my @pinfo = ();
    foreach my $iface (uniqueList(@ifaces)) {
	push @pinfo, { "name" => "$iface", "enabled" => $state };
    }

    my $porthash = {
	"ietf-interfaces:interfaces" => {
	    "interface" => \@pinfo
	}
    };
    return $porthash;
}

#
# PTP support. Here are some potentially useful PTP REST queries as returned
# by cli mode rest-translate:
#
# "show ptp":
#   dell-ptp:ptp-ds/clock-ds
#
# "show running-configuration interface ethernet 1/1/1:1":
#   ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1:1?content=config
#
# "show ptp interface ethernet 1/1/1:1":
#   ietf-interfaces:interfaces-state/interface=ethernet1%2F1%2F1:1/dell-ptp:ptp-port-ds/dell-ptp:port-ds
#
# "ptp enable" on a port:
#   -d '{"ietf-interfaces:interfaces":{"interface":[{"name":"ethernet1/1/1:1","dell-ptp:ptp-port-config":{"enable":true}}]}}' -X PATCH https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces
#
# "no ptp enable" on a port:
#   DELETE https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1:1/dell-ptp:ptp-port-config/dell-ptp:enable
#
# "ptp transport layer2" on a port:
#   -d '{"ietf-interfaces:interfaces":{"interface":[{"name":"ethernet1/1/1:1","dell-ptp:ptp-port-config":{"transport":{"layer2-mode":{"layer2":true}}}}]}}' -X PATCH https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces
#
# "no ptp transport":
#   DELETE https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1:1/dell-ptp:ptp-port-config/dell-ptp:transport
#
# "ptp role master" on a port:
#   -d '{"ietf-interfaces:interfaces":{"interface":[{"name":"ethernet1/1/1:1","dell-ptp:ptp-port-config":{"role":"master"}}]}}' -X PATCH https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces
#
# "no ptp role":
#   DELETE https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1:1/dell-ptp:ptp-port-config/dell-ptp:role
#
# Set ptp state for multiple ports:
#   curl -i -k -H "Accept: application/json" -H "Content-Type: application/json" -u $USER_NAME:$PASSWORD -d '{"ietf-interfaces:interfaces":{"interface":[{"name":"ethernet1/1/12","dell-ptp:ptp-port-config":{"enable":true,"role":"master","transport":{"layer2-mode":{"layer2":true}}}}]}}' -X PATCH https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces
#
# Clear most ptp state for multiple ports
# (transport must be cleared seperately):
#   curl -i -k -H "Accept: application/json" -H "Content-Type: application/json" -u $USER_NAME:$PASSWORD -d '{"ietf-interfaces:interfaces":{"interface":[{"name":"ethernet1/1/12","dell-ptp:ptp-port-config":{"enable":false,"role":"dynamic"}}]}}' -X PATCH https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces
#
# Set ptp state for single port:
#   curl -s -k -H "Accept: application/json" -H "Content-Type: application/json" -u $USER_NAME:$PASSWORD -d '{"dell-ptp:ptp-port-config":{"enable":true,"role":"master","transport":{"layer2-mode":{"layer2":true}}}}' -X PUT https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1/dell-ptp:ptp-port-config
#
# Clear ptp state for single port:
#   curl -s -k -H "Accept: application/json" -H "Content-Type: application/json" -u $USER_NAME:$PASSWORD -X DELETE https://$MGMT_IP/restconf/data/ietf-interfaces:interfaces/interface=ethernet1%2F1%2F1/dell-ptp:ptp-port-config
#

#
# Enable/diable PTP on a single port
# XXX role and transport ignored for now.
#
sub ptpPortSpec($$$$$)
{
    my ($self,$enable,$role,$transport,$iface) = @_;
    my $porthash;

    # This only makes sense for enable
    if ($enable) {
	$porthash = {
	    "dell-ptp:ptp-port-config" => {
		"enable" => JSON::PP::true,
		"role" => "master",
		"transport" => {
		    "layer2-mode" => {
			"layer2" => JSON::PP::true
		    }
		}
	    }
	};
    }

    return $porthash;
}

#
# Enable/diable PTP on multiple ports
# XXX role and transport ignored for now.
#
sub ptpMultiplePortSpec($$$$@)
{
    my ($self,$enable,$role,$transport,@ifaces) = @_;
    my $pconfig;

    if ($enable) {
	$pconfig = {
	    "enable" => JSON::PP::true,
	    "role" => "master",
	    "transport" => {
		"layer2-mode" => {
		    "layer2" => JSON::PP::true
		}
	    }
	};
    } else {
	$pconfig = {
	    "enable" => JSON::PP::false,
	    "role" => "dynamic",
	    "transport" => {}
	}
    }

    my @pinfo = ();
    foreach my $iface (uniqueList(@ifaces)) {
	push @pinfo, {
	    "name" => $iface,
	    "dell-ptp:ptp-port-config" => $pconfig
	};
    }

    my $porthash = {
	"ietf-interfaces:interfaces" => {
	    "interface" => \@pinfo
	}
    };

    return $porthash;
}

#
# Enable/diable SyncE on a single port
# XXX param is ignored.
#
sub syncePortSpec($$$$)
{
    my ($self,$enable,$param,$iface) = @_;
    my $porthash;

    # This only makes sense for enable
    if ($enable) {
	$porthash = {
	    "dell-synce:reference-config" => {
		"enable" => JSON::PP::true,
		"esmc-mode" => "tx-only"
	    }
	};
    }

    return $porthash;
}

#
# Handle serialization of calls to a switch. The switch itself will queue
# things, but it might cause a call to take a long time or a connection to
# timeout, so we block here instead.
#
# Returns 0 if we get the lock, non-zero otherwise.
#
my $lock_held = 0;

sub lock($) {
    my $self = shift;
    my $token = "dellrest_" . $self->{NAME};
    # XXX longest single call is around 10s, most are about 2s.
    my $timo = 60;
    my $rv = 0;

    if ($lock_held == 0) {
	my $old_umask = umask(0);
	$rv = TBScriptLock($token, 0, $timo);
	umask($old_umask);
    }
    if ($rv == 0) {
	$lock_held = 1;
    }

    return $rv;
}

sub unlock($) {
    if ($lock_held == 1) {
	TBScriptUnlock();
    }
    $lock_held = 0;
}

# End with true
1;
