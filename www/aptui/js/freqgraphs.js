//
// Frequency graphs, data from the monitors on the FEs/MEs/BSs.
//
// This code is mostly stolen from various example graphs on the D3
// tutorial website. 
//
$(function () {
window.ShowFrequencyGraph = (function ()
{
    'use strict';
    var d3 = d3v5;

    // Needed for the floating popovers, see below.
    // See https://popper.js.org/docs/v2/virtual-elements/ for an
    // explaination of this stuff. 
    var generateGetBoundingClientRect =
	function(x1 = 0, y1 = 0, x2 = 0, y2 = 0) {
	    return () => ({
		width: 10,
		height: 10,
		left: x1 - 5,
		top: y1 - 5,
		right: x1 + 5,
		bottom: y1 + 5,
	    });
	};

    function CreateGraph(args, data) {
	//console.log(data);
	
	var selector     = args.selector + " .frequency-graph-subgraph";
	// Closest positioned element.
	var parent       = $(selector).closest(".panel");
	var parentWidth  = $(parent).width();
	var parentHeight = $(parent).height();
	var ParentTop    = $(parent).position().top;
	var ParentLeft   = $(parent).position().left;
	// Not all data files have the incident value.
	var hasIncident  = (_.has(data[0], "incident") ? true : false);
	// Ditto the above noise floor values
	var hasAboveFloor= (_.has(data[0], "abovefloor") ? true : false);
	var lineI;

	// incident reporting is by request
	if (! args.incident) {
	    hasIncident = false;
	}

	var margin  = {top: 20, right: 20, bottom: 130, left: 55};
	var width   = parentWidth - margin.left - margin.right;
	var height  = parentHeight - margin.top - margin.bottom;
	var margin2 = {top: parentHeight - 80,
		       right: 20, bottom: 30, left: 55};
	var height2 = parentHeight - margin2.top - margin2.bottom;

	// Clear old graph
	$(selector).html("");

	console.info(margin, margin2);
	console.info(width, height, height2);

	var bisector = d3.bisector(function(d) { return d.frequency; }).left;
	var formatter = d3.format(".3f");

	var x = d3.scaleLinear().range([0, width]),
	    x2 = d3.scaleLinear().range([0, width]),
	    y = d3.scaleLinear().range([height, 0]),
	    y2 = d3.scaleLinear().range([height2, 0]);

	var xAxis = d3.axisBottom(x),
	    xAxis2 = d3.axisBottom(x2),
	    yAxis = d3.axisLeft(y);

	var brush = d3.brushX()
	    .extent([[0, 0], [width, height2]])
	    .on("brush end", brushed);

	var zoom = d3.zoom()
	    .scaleExtent([1, Infinity])
	    .translateExtent([[0, 0], [width, height]])
	    .extent([[0, 0], [width, height]])
	    .on("zoom", zoomed);

	var line = d3.line().curve(d3.curveStep)
            .x(function (d) { return x(d.frequency); })
            .y(function (d) { return y(d.power); });

	var line2 = d3.line().curve(d3.curveStep)
            .x(function (d) { return x2(d.frequency); })
            .y(function (d) { return y2(d.power); });

	if (hasIncident) {
	    lineI = d3.line().curve(d3.curveStep)
		.x(function (d) { return x(d.frequency); })
		.y(function (d) { return y(d.incident); });
	}
	
	var svg = d3.select(selector)
	    .append('svg')
            .attr("width", $(selector).width())
            .attr("height", $(selector).height());
	
	var clip = svg.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("svg:rect")
            .attr("width", width)
            .attr("height", height)
            .attr("x", 0)
            .attr("y", 0); 

	var Line_chart = svg.append("g")
            .attr("class", "focus")
            .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")")
	    .attr("clip-path", "url(#clip)");

	var focus = svg.append("g")
            .attr("class", "focus")
            .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")");

	var context = svg.append("g")
	    .attr("class", "context")
	    .attr("transform",
		  "translate(" + margin2.left + "," + margin2.top + ")");

	x.domain(d3.extent(data, function(d) { return d.frequency; }));
	// I want a little more pad above and below
	var power_extents = d3.extent(data, function(d) { return d.power; });
	console.info("power extents", power_extents);
	if (hasIncident) {
	    var incident_extents = d3.extent(data, function(d) { return d.incident; });
	    console.info("incident_extents", incident_extents);
	    if (incident_extents[0] < power_extents[0]) {
		power_extents[0] = incident_extents[0];
	    }
	    if (incident_extents[1] > power_extents[1]) {
		power_extents[1] = incident_extents[1];
	    }
	}
	console.info("power extents", power_extents);
	power_extents[0] = power_extents[0] - 2;
	power_extents[1] = power_extents[1] + 2;
	console.info(power_extents);
	y.domain(power_extents);
	x2.domain(x.domain());
	y2.domain(y.domain());

	focus.append("g")
	    .attr("class", "axis axis--x")
	    .attr("transform", "translate(0," + height + ")")
	    .call(xAxis);

	// text label for the x axis
	focus.append("text")             
	    .attr("y", height + margin.top + 15)
	    .attr("x", (width / 2))
	    .style("text-anchor", "middle")
	    .text("Frequency (MHz)");

	focus.append("g")
	    .attr("class", "axis axis--y")
	    .call(yAxis);

	// text label for the y axis
	focus.append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 0 - margin.left)
	    .attr("x",0 - (height / 2))
	    .attr("dy", "1em")
	    .style("text-anchor", "middle")
	    .text("Power (dB)");
	
	Line_chart.append("path")
	    .datum(data)
	    .attr("class", "line line-power")
	    .attr("d", line);

	if (hasAboveFloor) {
	    Line_chart.selectAll("myCircles")
		.data(data.filter(function(d) { return d.abovefloor != 0; } ))
		.enter()
		.append("circle")
		.attr("class", "abovefloor-circles")
		.attr("fill", function(d) {
		    return d.violation ? "red" : "blue"; })
		.attr("stroke", "none")
		.attr("cx", function(d) { return x(d.frequency) })
		.attr("cy", function(d) { return y(d.power) })
		.attr("r", 4);
	}

	if (hasIncident) {
	    Line_chart.append("path")
		.datum(data)
		.attr("class", "line line-incident")
		.attr("d", lineI);
	}

	var tooltip = Line_chart.append("g")
	    .attr("class", "tooltip")
	    .style("opacity", "1.0")
	    .style("display", "none");

	tooltip.append("circle")
	    .attr("r", 5);

	const virtualElement = {
	    getBoundingClientRect: generateGetBoundingClientRect(),
	};
	
	$('#subgraph-tooltip-popover')
	    .popover({"content"   : $("#subgraph-tooltipTemplate").html(),
		      "template"  : $("#popover-template").html(),
		      "trigger"   : "manual",
		      "html"      : true,
		      "container" : $(parent)[0],
		      "placement" : "left",
		      // These are popper config variables.
		      "fallbackPlacements" : ["right"],
		      "reference" : virtualElement,
		     });

	function HideTooltip()
	{
	    // The circle
	    tooltip.style("display", "none");;
	    // The box
	    $('#subgraph-tooltip-popover').popover("hide");	    
	}

	function ShowTooltip()
	{
	    // The circle.
	    tooltip.style("display", null);
	}
    
	context.append("path")
	    .datum(data)
	    .attr("class", "line")
	    .attr("d", line2);

	context.append("g")
	    .attr("class", "axis axis--x")
	    .attr("transform", "translate(0," + height2 + ")")
	    .call(xAxis2);

	context.append("g")
		    .attr("class", "brush")
	    .call(brush)
	    .call(brush.move, x.range());

	svg.append("rect")
	    .attr("class", "zoom")
	    .attr("width", width)
	    .attr("height", height)
	    .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")")
	    .call(zoom)
	    .on("mouseover", ShowTooltip)
	    .on("mouseout", HideTooltip)
	    .on("mousemove", mousemove);

	function mousemove() {
	    var x0 = x.invert(d3.mouse(this)[0]),
		i = bisector(data, x0, 1),
		d0 = data[i - 1],
		d1 = data[i];
	    
	    var d = x0 - d0.frequency > d1.frequency - x0 ? d1 : d0;
	    //console.info(x0, d, x(d.frequency));

	    tooltip.attr("transform",
			 "translate(" + x(d.frequency) +
			 "," + y(d.power) + ")");

	    // Bootstrap popover based tooltip.
	    // Bootstrap popover based tooltip.
	    var isVisible = false;
	    var popoverid = $('#subgraph-tooltip-popover')
		.attr("aria-describedby");
	    if (popoverid && $("#" + popoverid).length) {
		isVisible = true;
	    }
	    var updater   = function () {
		var content = $('#' + popoverid).find(".popover-body");

		$(content).find(".tooltip-freq")
		    .html(formatter(d.frequency));
		$(content).find(".tooltip-power")
		    .html(formatter(d.power));
		if (_.has(d, "center_freq")) {
		    $(content).find(".tooltip-center")
			.html(formatter(d.center_freq));
		}
		else {
		    $(content).find(".tooltip-center").text("n/a");
		}
		if (hasIncident && _.has(d, "incident")) {
		    $(content).find(".tooltip-incident .incident")
			.html(formatter(d.incident));
		    $(content).find(".tooltip-incident")
			.removeClass("hidden");
		}
		if (hasAboveFloor) {
		    if (d.abovefloor) {
			$(content).find(".tooltip-abovefloor .abovefloor")
			    .html(formatter(d.abovefloor) + " dB");
			$(content).find(".tooltip-abovefloor")
			    .removeClass("hidden");
		    }
		    else {
			$(content).find(".tooltip-abovefloor")
			    .addClass("hidden");
		    }
		}
		var ptop    = Math.floor(ParentTop + y(d.power));
		var pleft   = Math.floor(ParentLeft + x(d.frequency)) + margin.left;
		// And compensate for scroll.
		ptop -= $(window).scrollTop();
		
		virtualElement.getBoundingClientRect =
		    generateGetBoundingClientRect(pleft, ptop);
		
		$('#subgraph-tooltip-popover').popover('update');
	    };
	    if (isVisible) {
		updater();
	    }
	    else {
		$('#subgraph-tooltip-popover')
		    .one("inserted.bs.popover", function (event) {
			updater();
		    });
		$('#subgraph-tooltip-popover').popover('show');
	    }
	}

	function brushed() {
	    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom")
		return; // ignore brush-by-zoom
	    var s = d3.event.selection || x2.range();
	    x.domain(s.map(x2.invert, x2));
	    Line_chart.select(".line-power").attr("d", line);
	    if (hasIncident) {
		Line_chart.select(".line-incident").attr("d", lineI);
	    }
	    if (hasAboveFloor) {
		Line_chart.selectAll(".abovefloor-circles")
		    .attr("cx", function(d) { return x(d.frequency) });
	    }
	    focus.select(".axis--x").call(xAxis);
	    svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
				     .scale(width / (s[1] - s[0]))
				     .translate(-s[0], 0));
	}

	function zoomed() {
	    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush")
		return; // ignore zoom-by-brush
	    var t = d3.event.transform;
	    x.domain(t.rescaleX(x2).domain());
	    Line_chart.select(".line-power").attr("d", line);
	    if (hasIncident) {
		Line_chart.select(".line-incident").attr("d", lineI);
	    }
	    if (hasAboveFloor) {
		Line_chart.selectAll(".abovefloor-circles")
		    .attr("cx", function(d) { return x(d.frequency) });
	    }
	    focus.select(".axis--x").call(xAxis);
	    context.select(".brush")
		.call(brush.move, x.range().map(t.invertX, t));
	}
    }

    function CreateBins(data)
    {
	var result = [];
	var bins   = [];
	var hasAboveFloor= (_.has(data[0], "abovefloor") ? true : false);
	console.info("CreateBins: ", data);

	_.each(data, function (d, index) {
	    var freq  = +d.frequency;
	    var power = +d.power;
	    var x     = Math.floor(freq);

	    if (!_.has(bins, x)) {
		var bin = {
		    "frequency" : x,
		    "max"       : power,
		    "min"       : power,
		    "avg"       : power,
		    "samples"   : [d],
		};
		if (hasAboveFloor) {
		    bin["abovefloor"] = d.abovefloor;
		    bin["violation"]  = d.violation;
		    if (d.abovefloor) {
			console.info(bin);
		    }
		}
		bins[x] = bin;
		result.push(bin);
		return;
	    }
	    var bin = bins[x];
	    if (power > bin.max) {
		bin.max = power;
	    }
	    if (power < bin.min) {
		bin.min = power;
	    }
	    if (hasAboveFloor) {
		if (d.abovefloor > bin.abovefloor) {
		    bin.abovefloor = d.abovefloor;
		}
		if (d.violation) {
		    bin.violation = 1;
		}
		if (d.abovefloor) {
		    console.info(bin);
		}
	    }
	    bin.samples.push(d);
	    var sum = 0;
	    _.each(bin.samples, function (d) {
		sum  += d.power;
	    });
	    bin.avg  = sum / _.size(bin.samples);
	});
	//console.info("bins", result);
	return result;
    }

    function CreateBinGraph(args, data) {
	var bins         = CreateBins(data);
	var selector     = args.selector + " .frequency-graph-maingraph";
	// Closest positioned element.
	var parent       = $(selector).closest(".panel");
	var parentWidth  = $(parent).width();
	var parentHeight = $(parent).height();
	var ParentTop    = $(parent).position().top;
	var ParentLeft   = $(parent).position().left;
	var hasAboveFloor= (_.has(data[0], "abovefloor") ? true : false);
	var lineI;

	// Clear old graph
	$(selector).html("");
	// And the sub graph.
	$(args.selector + " .frequency-graph-subgraph").html("");
	
	var margin  = {top: 20, right: 20, bottom: 130, left: 55};
	var width   = parentWidth - margin.left - margin.right;
	var height  = parentHeight - margin.top - margin.bottom;
	var margin2 = {top: parentHeight - 80,
		       right: 20, bottom: 30, left: 55};
	var height2 = parentHeight - margin2.top - margin2.bottom;

	console.info(margin, margin2);
	console.info(parentWidth, parentHeight, ParentTop, ParentLeft);
	console.info(width, height, height2);

	var bisector = d3.bisector(function(d) { return d.frequency; }).left;
	var formatter = d3.format(".3f");

	var x = d3.scaleLinear().range([0, width]),
	    x2 = d3.scaleLinear().range([0, width]),
	    y = d3.scaleLinear().range([height, 0]),
	    y2 = d3.scaleLinear().range([height2, 0]);

	var xAxis = d3.axisBottom(x),
	    xAxis2 = d3.axisBottom(x2),
	    yAxis = d3.axisLeft(y);

	var brush = d3.brushX()
	    .extent([[0, 0], [width, height2]])
	    .on("brush end", brushed);

	var zoom = d3.zoom()
	    .scaleExtent([1, Infinity])
	    .translateExtent([[0, 0], [width, height]])
	    .extent([[0, 0], [width, height]])
	    .on("zoom", zoomed);

	var line = d3.line().curve(d3.curveStep)
            .x(function (d) { return x(d.frequency); })
            .y(function (d) { return y(d.max); });

	var line2 = d3.line().curve(d3.curveStep)
            .x(function (d) { return x2(d.frequency); })
            .y(function (d) { return y2(d.max); });

	var svg = d3.select(selector)
	    .append('svg')
            .attr("width", $(selector).width())
            .attr("height", $(selector).height());
	
	var clip = svg.append("defs").append("svg:clipPath")
            .attr("id", "clip")
            .append("svg:rect")
            .attr("width", width)
            .attr("height", height)
            .attr("x", 0)
            .attr("y", 0); 

	var Line_chart = svg.append("g")
            .attr("class", "focus")
            .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")")
	    .attr("clip-path", "url(#clip)");

	var focus = svg.append("g")
            .attr("class", "focus")
            .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")");

	var context = svg.append("g")
	    .attr("class", "context")
	    .attr("transform",
		  "translate(" + margin2.left + "," + margin2.top + ")");

	x.domain(d3.extent(bins, function(d) { return d.frequency; }));
	y.domain(d3.extent(bins, function(d) { return d.max + 1; }));
	x2.domain(x.domain());
	y2.domain(y.domain());

	focus.append("g")
	    .attr("class", "axis axis--x")
	    .attr("transform", "translate(0," + height + ")")
	    .call(xAxis);

	// text label for the x axis
	focus.append("text")             
	    .attr("y", height + margin.top + 15)
	    .attr("x", (width / 2))
	    .style("text-anchor", "middle")
	    .text("Frequency (MHz)");

	focus.append("g")
	    .attr("class", "axis axis--y")
	    .call(yAxis);

	// text label for the y axis
	focus.append("text")
	    .attr("transform", "rotate(-90)")
	    .attr("y", 0 - margin.left)
	    .attr("x",0 - (height / 2))
	    .attr("dy", "1em")
	    .style("text-anchor", "middle")
	    .text("Power (dB)");      
	
	Line_chart.append("path")
	    .datum(bins)
	    .attr("class", "line line-power")
	    .attr("d", line);

	if (hasAboveFloor) {
	    Line_chart.selectAll("myCircles")
		.data(bins.filter(function(d) { return d.abovefloor != 0; } ))
		.enter()
		.append("circle")
		.attr("class", "abovefloor-circles")
		.attr("fill", function(d) { return d.violation ? "red" : "blue";})
		.attr("stroke", "none")
		.attr("cx", function(d) { return x(d.frequency) })
		.attr("cy", function(d) { return y(d.max) })
		.attr("r", 4);
	}

	var tooltip = Line_chart.append("g")
	    .attr("class", "tooltip")
	    .style("opacity", "1.0")
	    .style("display", "none");

	tooltip.append("circle")
	    .attr("r", 5);

	context.append("path")
	    .datum(bins)
	    .attr("class", "line")
	    .attr("d", line2);

	context.append("g")
	    .attr("class", "axis axis--x")
	    .attr("transform", "translate(0," + height2 + ")")
	    .call(xAxis2);

	context.append("g")
	    .attr("class", "brush")
	    .call(brush)
	    .call(brush.move, x.range());

	svg.append("rect")
	    .attr("class", "zoom")
	    .attr("width", width)
	    .attr("height", height)
	    .attr("transform",
		  "translate(" + margin.left + "," + margin.top + ")")
	    .call(zoom)
	    .on("mouseover", ShowTooltip)
	    .on("mouseout", HideTooltip)
	    .on("mousemove", mousemove)
	    .on("click", DrawSubGraph);

	const virtualElement = {
	    getBoundingClientRect: generateGetBoundingClientRect(),
	};
	
	$('#maingraph-tooltip-popover')
	    .popover({"content"   : $("#maingraph-tooltipTemplate").html(),
		      "template"  : $("#popover-template").html(),
		      "trigger"   : "manual",
		      "html"      : true,
		      "container" : $(parent)[0],
		      "placement" : "left",
		      // These are popper config variables.
		      "fallbackPlacements" : ["right"],
		      "reference" : virtualElement,
		     });

	function HideTooltip()
	{
	    // The circle
	    tooltip.style("display", "none");;
	    // The box
	    $('#maingraph-tooltip-popover').popover("hide");	    
	}

	function ShowTooltip()
	{
	    // The circle.
	    tooltip.style("display", null);
	}
    
	function mousemove() {
	    //console.info(d3.event, d3.mouse(this));
	    
	    var x0 = x.invert(d3.mouse(this)[0]),
		i = bisector(bins, x0, 1),
		d0 = bins[i - 1],
		d1 = bins[i];

	    var d = x0 - d0.frequency > d1.frequency - x0 ? d1 : d0;
	    //console.info(x0, d, x(d.frequency));
	    //console.info(x(d.frequency), y(d.avg));

	    tooltip.attr("transform",
			 "translate(" + x(d.frequency) +
			 "," + y(d.max) + ")");

	    // Bootstrap popover based tooltip.
	    var isVisible = false;
	    var popoverid = $('#maingraph-tooltip-popover')
		.attr("aria-describedby");
	    if (popoverid && $("#" + popoverid).length) {
		isVisible = true;
	    }

	    var updater   = function () {
		var content = $('#' + popoverid).find(".popover-body");

		$(content).find(".tooltip-frequency")
		    .html(formatter(d.frequency));
		$(content).find(".tooltip-min")
		    .html(formatter(d.min));
		$(content).find(".tooltip-max")
		    .html(formatter(d.max));
		$(content).find(".tooltip-avg")
		    .html(formatter(d.avg));
		if (hasAboveFloor) {
		    if (d.abovefloor) {
			$(content).find(".tooltip-abovefloor .abovefloor")
			    .html(formatter(d.abovefloor) + " dB");
			$(content).find(".tooltip-abovefloor")
			    .removeClass("hidden");
		    }
		    else {
			$(content).find(".tooltip-abovefloor")
			    .addClass("hidden");
		    }
		}
		var ptop    = Math.floor(ParentTop + y(d.max));
		var pleft   = Math.floor(ParentLeft + x(d.frequency)) + margin.left;
		// And compensate for scroll.
		ptop -= $(window).scrollTop();
		
		virtualElement.getBoundingClientRect =
		    generateGetBoundingClientRect(pleft, ptop);
		
		$('#maingraph-tooltip-popover').popover('update');
	    };
	    if (isVisible) {
		updater();
	    }
	    else {
		$('#maingraph-tooltip-popover')
		    .one("inserted.bs.popover", function (event) {
			updater();
		    });
		$('#maingraph-tooltip-popover').popover('show');
	    }
	}

	function brushed() {
	    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "zoom")
		return; // ignore brush-by-zoom
	    var s = d3.event.selection || x2.range();
	    x.domain(s.map(x2.invert, x2));
	    Line_chart.select(".line-power").attr("d", line);
	    if (hasAboveFloor) {
		Line_chart.selectAll(".abovefloor-circles")
		    .attr("cx", function(d) { return x(d.frequency) });
	    }
	    focus.select(".axis--x").call(xAxis);
	    svg.select(".zoom").call(zoom.transform, d3.zoomIdentity
				     .scale(width / (s[1] - s[0]))
				     .translate(-s[0], 0));
	}

	function zoomed() {
	    if (d3.event.sourceEvent && d3.event.sourceEvent.type === "brush")
		return; // ignore zoom-by-brush
	    var t = d3.event.transform;
	    x.domain(t.rescaleX(x2).domain());
	    Line_chart.select(".line-power").attr("d", line);
	    if (hasAboveFloor) {
		Line_chart.selectAll(".abovefloor-circles")
		    .attr("cx", function(d) { return x(d.frequency) });
	    }
	    focus.select(".axis--x").call(xAxis);
	    context.select(".brush")
		.call(brush.move, x.range().map(t.invertX, t));
	}
	/*
	 * Draw zoomed graph in lower panel, after user clicks on a point.
	 */
	function DrawSubGraph()
	{
	    var x0   = x.invert(d3.mouse(this)[0]);
	    var i    = bisector(bins, x0, 1);
	    var d    = bins[i];
	    var freq = d.frequency;
	    var subdata = [];
	    var index   = (i < 25 ? 0 : i - 25);

	    for (i = index; i < index + 50; i++) {
		// Hmm, the CSV file appears to not be well sorted within
		// a frequency bin. Must be a string sort someplace.
		var sorted = bins[i].samples
		    .sort(function (a, b) { return a.frequency - b.frequency});
		subdata = subdata.concat(sorted);
	    }
	    CreateGraph(args, subdata);
	}
	/*
	 * Draw a zoomed graph after user searches for min/max
	 */
	$(args.selector + " .frequency-search button").off("click");
	$(args.selector + " .frequency-search button").click(ZoomToSubGraph);
	
	function ZoomToSubGraph()
	{
	    var min = $.trim($(args.selector + " .min-freq-input").val());
	    var max = $.trim($(args.selector + " .max-freq-input").val());
	    
	    if (min == "" || max == "") {
		return;
	    }
	    var extents = d3.extent(bins, function(d) { return d.frequency; });
	    if (min < extents[0] || max > extents[1]) {
		alert("Search out of range: " + extents[0] + "," + extents[1]);
		return;
	    }
	    x.domain(extents);
	    console.info("ZoomToSubGraph", min, max, extents);
	    console.info(x(min), x(max));
	    context.select(".brush").call(brush.move, [x(min), x(max)]);
	}
    }
    function type(d) {
	d.frequency = +d.frequency;
	d.power     = +d.power;
	if (_.has(d, "center_freq")) {
	    d.center_freq = +d.center_freq;
	}
	if (_.has(d, "incident")) {
	    d.incident = +d.incident;
	}
	if (_.has(d, "abovefloor") && d.abovefloor != "") {
	    d.abovefloor = +d.abovefloor;
	}
	else {
	    d.abovefloor = 0;
	}
	if (_.has(d, "violation") && d.abovefloor != "") {
	    d.violation = +d.violation;
	}
	else {
	    d.violation = 0;
	}
	return d;
    }

    // Easier to get a binary (gzip) file this way, since jquery does
    // not directly support doing this. 
    function GetBlob(url, success, failure) {
	var oReq = new XMLHttpRequest();
	oReq.open("GET", url, true);
	oReq.responseType = "arraybuffer";

	oReq.onload = function(oEvent) {
	    success(oReq.response)
	};
	oReq.onerror = function(oEvent) {
	    failure();
	};
	oReq.send();
    }

    function getRandomInt() {
	var min = 10000;
	var max = 99999999;
	
	return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    // Link to the graph page for a specific graph.
    function GraphURL(args, info)
    {
	console.info("GraphURL", info);
	
	if (_.has(info, "graphurl")) {
	    return info.graphurl;
	}
	var dirname = info["dirname"];
	
	var url = window.location.origin + "/" +
	    window.location.pathname + "?logid=" + info.logid +
	    "&node_id=" + info.node_id +
	    "&iface=" + info.iface;

	if (args.which == "rfmonitor") {
	    if (args.cluster) {
		url = url + "&cluster=" + args.cluster;
	    }
	}
	else {
	    var endpoint;
	    if (args.endpoint) {
		endpoint = args.endpoint;
	    }
	    else {
		// Get it from the path. Do not like this.
		var dirs = info.path.split('/').reverse();
		
		if (info.archived) {
		    endpoint = dirs[1];
		}
		else {
		    endpoint = dirs[0];
		}
	    }
	    url = url + "&endpoint=" + endpoint;
	}
	url = url + "&which=" + args.which;
	if (dirname == "archive") {
	    url = url + "&archived=1";
	}
	// Remember it so we can add a link at top of page when selected
	info["graphurl"] = url;
	return url;
    }

    function BuildMenu(args)
    {
	// the graph we want to display (if specified).
	var display = null;
	var latest  = null;
	// nuc2:rf0-1588699912.csv.gz
	var re1 = /([^:]+):([^\-]+)\-(\d+)\.csv\.gz/;

	// Process the returned list of files and directories.
	var processDir = function (path, dirname, dirlist) {
	    var path = path + "/" + dirname;
	    console.info(path, dirname, dirlist);
	    
	    // Prune the csb files then sort them by the timestamp
	    var files   = [];
	    // Directories go at the top.
	    var dirs    = [];
	    // If more then one node, then a directory for each node.
	    var nodes   = {};
	    
	    _.each(dirlist, function(info, index) {
		var name    = info.name;
		var logid   = null;
		var match   = name.match(re1);
		var node_id = null;

		// Process a subdir.
		if (_.has(info, "subdir")) {
		    if (_.size(info.subdir)) {
			var menu = processDir(path, info.name, info.subdir);
			if (_.size(menu)) {
			    info.submenu = menu;
			    dirs.push(info);
			}
		    }
		    return;
		}
		//console.info(name, match);
		if (!match) {
		    return;
		}
		var logid = parseInt(match[3]);

		// Prune to range.
		if (_.has(args, "rangestart") && logid < args.rangestart) {
		    return;
		}
		if (_.has(args, "rangeend") && logid > args.rangeend) {
		    return;
		}
		
		// Prune out other radios and interfaces unless browsing
		if (args.which == "rfbaseline") {
		    info["node_id"] = node_id = match[1];
		    info["iface"]   = match[2];
		}
		else {
		    if ((args.node_id && match[1] != args.node_id) ||
			(args.iface && match[2] != args.iface)) {
			return;
		    }
		    info["node_id"] = node_id = match[1];
		    info["iface"]   = match[2];
		}
		info["path"]      = path;
		info["logid"]     = logid;
		info["id"]        = getRandomInt();
		info["lastmod"]   = parseInt(info["lastmod"]);
		info["archived"]  = dirname == "archive" ? 1 : 0;

		if (!_.has(nodes, node_id)) {
		    nodes[node_id] = [];
		}
		nodes[node_id].push(info);
	    });
	    if (! (_.size(nodes) || _.size(dirs))) {
		return;
	    }
	    // Build the menu for this level. Directories first.
	    var menu = $("<ul class='dropdown-menu'></ul>");
	    
	    // Directories alphabetically.
	    if (_.size(dirs)) {
		dirs.sort(function (a, b) {
		    if (a.name < b.name) {return -1;}
		    if (a.name > b.name) {return 1;}		    
		    return 0;
		});
		_.each(dirs, function(info) {
		    var item =
			$("<li class='dropstart " +
			  "           multilevel-toggle'>" +
			  "  <a href='#' " +
			  "     class='dropdown-item dropdown-toggle' " +
			  "     data-bs-auto-close='false' " +
			  "     data-bs-toggle='dropdown'>" +
			     info.name + "</a>" +
			  "</li>");
		    $(item).append(info.submenu);
		    $(menu).append(item);
		});
	    }
	    // Sort and build a list for each node. Might be only one node.
	    _.each(nodes, function(list, node_id) {
		var menuitems = [];

		// Sort files by timestamp.
		list.sort(function (a, b) {
		    var atime = (a.logid ? a.logid : a.lastmod);
		    var btime = (b.logid ? b.logid : b.lastmod);

		    return btime - atime;
		});
		_.each(list, function(info) {
		    // Remember this for generating graph url.
		    info["dirname"] = dirname;
		    
		    var html =
			"<li class='fgraph-" + info.id  + "'>" +
			" <a href='#' class='dropdown-item'>" +
			info.node_id + ":" + info.iface + " - " +
			moment(info.logid ?
			       info.logid : info.lastmod, "X").format("L LTS") +
			"</a></li>";
		    var item = $(html);
		    $(item).click(function (event) {
			event.preventDefault();
			ClearDropdowns();
			UpdateGraph(args, info);
		    });
		    // Lazily put in the href for the specific graph link.
		    $(item).hover(function (event) {
			var url = GraphURL(args, info);
			$(this).find("a").attr("href", url);
		    });
		    menuitems.push(item);

		    // Watch for the one we want to display.
		    if (args.logid) {
			if (info.logid == args.logid &&
			    info.node_id == args.node_id &&
			    info.iface == args.iface) {
			    display = info;
			}
		    }
		    // Latest graph will be shown if nothing else.
		    if (dirname != "archive" && 
			(!latest || info.logid > latest.logid)) {
			latest = info;
		    }
		});
		if (_.size(nodes) > 1) {
		    var item =
			$("<li class='dropstart multilevel-toggle'>" +
			  "  <a href='#' " +
			  "     class='dropdown-item dropdown-toggle' " +
			  "     data-bs-auto-close='false' " +
			  "     data-bs-toggle='dropdown'> " +
			        node_id + "</a>" +
			  "  <ul class='dropdown-menu multilevel-scrollable'>" +
			  "     <li class='disabled text-center'>" +
			  "       <a href='#' class='dropdown-item'>" +
			        node_id + "</a></li>" +
			  "     <li class='divider' role='separator' " +
			  "         style='margin-top: 0;'>" +
			  "  </ul> " +
			  "</li>");
		    
		    $(item).find("ul").append(menuitems);
		    $(menu).append(item);
		}
		else {
		    $(menu).append(menuitems);
		}
	    });

	    //console.info(dirname, $(menu).html());
	    return menu;
	}

	/* Clear all open dropdowns when any graph is selected. */
	function ClearDropdowns()
	{
	    $(args.selector + ' .moregraphs-dropdown .dropdown-toggle.show')
		.dropdown('hide');
	}
	
	var callback = function (value) {
	    // XXX This will always be a string. Need to
	    // figure out how to deal with errors.
	    if (typeof(value) == "object") {
		console.info("Could not get listing data: " + value.value);
		return;
	    }
	    var listing = JSON.parse(_.unescape(value));

	    var menu = processDir("", "", listing);
	    
	    //console.info($(menu).html());
	    $(args.selector + ' .moregraphs-dropdown').append(menu);

	    /*
	     * Any click outside our multilevel dropdowns closes any
	     * open menus.
	     */
	    $('body').click(function (event) {
		var target = $(event.target);
		var moregraphs = $(target).closest(".moregraphs-dropdown");
		if (!moregraphs.length) {
		    ClearDropdowns();
		}
	    });

	    /*
	     * Anytime we click on (show) a menu, we want to hide the
	     * any sibling (and its children) that are showing. 
	     */
	    $(args.selector + ' .moregraphs-dropdown .multilevel-toggle')
		.click(function(event) {
		    console.info("multilevel-toggle", event);
		    console.info($(this), $(this).siblings());

		    _.each($(this).siblings(), function (sibling) {
			$(sibling).find('.dropdown-toggle.show')
			    .each(function () {
				$(this).dropdown('hide');
			    });
		    });
		    event.stopPropagation();
		});
	    
	    $(menu).find(".multilevel-menu-parent")
		.hover(
		    function(event) {
			// Offset of this menu item.
			var offset  = $(this).offset();
			// Offset of the menu.
			var poffset = $(this).closest(".dropdown-menu").offset();
			// Wrapper
			var wrapper = $(this).children(".dropdown");
			// Menu to be displayed
			var menu    = $(wrapper).children(".dropdown-menu");
		    
			console.info("offsets", offset, poffset);

			// Adjust the top of the menu.
			var height = $(menu).height();
			var top    = offset.top - poffset.top - 15;
			console.info("h/t", height, top);
			$(wrapper).css("top", top + "px");

			// Adjust the left offset of the menu. Oddly, it has to
			// to the left of the scrollbar or else the hover does
			// not work.
			var thiswidth = $(this).width();
			var menuwidth = $(menu).width();
			var left;

			console.info("widths", thiswidth, menuwidth,
				     $(window).width());
		    
			// Clear it so calculation below works right.
			$(wrapper).css("left", '')

			if (poffset.left + thiswidth + menuwidth + 30 >
			    $(window).width()) {
			    var left = 0 - menuwidth;      
			}
			else {
			    left = thiswidth;
			}
			left = poffset.left + left;
			console.info("left", poffset.left, left);
			$(wrapper).css("left", left + "px")
		    },
		    function(event) {
			var menu = $(event.target)
			    .parent().find(".dropdown-menu");
		    });
	    
	    if (display || latest) {
		UpdateGraph(args, display ? display : latest);
	    }
	    else {
		$(args.selector + " .frequency-graph-maingraph .spinner center")
		    .html("Please select a graph to view");
	    }
	};
	var url = args.url + "/" + args.which + "/";
	if (args.which != "rfmonitor") {
	    if (args.endpoint) {
		url = url + args.endpoint + "/";
	    }
	}
	url = url + "listing.php";
	if ((args.which == "rfmonitor" ||
	     args.which == "rfbaseline") && args.node_id) {
	    url = url + "?node_id=" + args.node_id;
	}
	console.info("BuildMenu", url);
	
	$.get(url, callback);
    }

    function SetupDownload(args, url)
    {
	console.info("Download", url);
	var selector = args.selector + " .download-button";

	$(selector)
	    .attr("href", url)
	    .removeAttr("disabled");
    }

    function SetGraphDetails(args, info)
    {
	// If no logid (timestamp) use the lastmod from the listing.
	var when = (info.logid ? info.logid : info.lastmod);
	var cluster = null;
		    
	$(args.selector + " .frequency-graph-date")
	    .html(moment(when, "X").format("L LTS"))
	    .removeClass("hidden");

	$(args.selector + " .frequency-graph-nodeid")
	    .html(info.node_id);

	$(args.selector + " .frequency-graph-iface")
	    .html(info.iface);

	if (args.cluster || args.endpoint) {
	    cluster = args.endpoint ? args.endpoint : args.cluster;
	}
	else if (_.has(info, "path")) {
	    cluster = info.path.split('/').reverse()[0];
	}
	if (cluster) {
	    $(args.selector + " .frequency-graph-cluster").html(cluster)
	}

	// GPS link
	if (args.which == "rfmonitor-mobile" && info.logid && cluster) {
	    var to     = info.logid;
	    var from   = (to - 75);
	    
	    var gpsurl = "https://overwatch.emulab.net:8889/" +
		"d/VY5WqX1Mz/mobile-endpoint?orgId=3" +
		"&from=" + (from * 1000) + "&to=" + (to * 1000) +
		"&var-fixedNode=" + cluster + "&viewPanel=9";
	    
	    $(args.selector + " .gps-button")
		.attr("href", gpsurl)
		.removeAttr("disabled");
	}
	
	$(args.selector + ' .moregraphs-dropdown')
	    .find(".active").removeClass("active");
	$(args.selector + ' .moregraphs-dropdown')
	    .find(".fgraph-" + info.id + " a").addClass("active");

	// Link to graph.
	var url = GraphURL(args, info);
	console.info(url);
	
	$(args.selector + ' .share-button')
	    .data("graphurl", url)
	    .removeAttr("disabled");
    }

    function UpdateGraph(args, info)
    {
	/*
	 * It is a little difficult to get binary data, not directly
	 * possible with jquery ajax call, so we have to something
	 * special.
	 */
	var url = args.url + "/" + args.which + "/";
	if (args.endpoint) {
	    url = url + args.endpoint + "/";
	}
	url = url + info["path"] + "/";
	url = url + info.node_id + ":" + info.iface;
	if (info.logid) {
	    url = url + "-" + info.logid;
	}
	url = url + ".csv.gz";
			   
	console.info("UpdateGraph", args, info, url);

	// Disable the download button until we have the data.
	$(args.selector + " .download-button").attr("disabled", "disabled");

	// Ditto the GPS link
	if (args.which == "rfmonitor-mobile") {
	    $(args.selector + " .gps-button").attr("disabled", "disabled");
	}

	// Clear the graph now and show the spinner.	
	$(args.selector + " .frequency-graph-maingraph").html("");
	$(args.selector + " .frequency-graph-subgraph").html("");

	// Throw in the spinner
	var spinner = $(args.selector + " .spinner").clone();
	$(spinner).removeClass("hidden");
	$(args.selector + " .frequency-graph-maingraph").append(spinner);

	GetBlob(url,
		function (arrayBuffer) {
		    console.info("gz version");
		    var output = pako.inflate(arrayBuffer, { 'to': 'string' });
		    
		    var data = d3.csvParse(output, type);
		    CreateBinGraph(args, data);
		    $(args.selector + " .spinner").addClass("hidden");
		    SetupDownload(args, url);
		    SetGraphDetails(args, info);
		},
		function () {
		    alert("Could not get data file: " + url);
		});
    }

    /*
     * Handle the Share button popup.
     */
    function Share(args)
    {
	var selector = args.selector + ' .share-button';
	var url = $(selector).data("graphurl");
	var id = "xxxyyy";
	var input = id + "-url-input";
	var copy  = id + "-url-copy";
	
	var popupstring = 
	    "<div style='width 100%'> "+
	    "  <input readonly type=text " +
	    "       id='" + input + "' " +
	    "       style='display:inline; width: 93%; padding: 2px;' " +
	    "       class='form-control input-sm' " +
	    "       value='" + url + "'>" +
	    "  <a href='#' class='btn' " +
	    "     id='" + copy + "' " +
	    "     style='padding: 0px'>" +
	    "    <span class='glyphicon glyphicon-copy'></span></a></div>";
	
	if ($("#" + input).length == 0) {
	    $(selector).popover({
		html:     true,
		content:  popupstring,
		trigger:  'manual',
		placement:'auto',
		container:'body',
	    });
	    $(selector).popover('show');
	    $('#' + copy).click(function (e) {
		e.preventDefault();
		$('#' + input).select();
		document.execCommand("copy");
		$(selector).popover('destroy');
	    });
	    $('#' + input).click(function (e) {
		e.preventDefault();
		$(selector).popover('destroy');
	    });
	}
	else {
	    $(selector).popover('destroy');
	}
    }

    return function(args) {
	$(args.selector + ' .share-button').click(function (e) {
	    Share(args);
	});
	BuildMenu(args);
	if (_.has(args, "enableReload") && args.enableReload) {
	    $(args.selector + ' .reload-button').click(function (e) {
		BuildMenu(args);
	    });
	    $(args.selector + ' .reload-button').removeClass("hidden");
	}
	$(args.selector + ' [data-toggle="tooltip"]').tooltip({
	    placement: 'auto',
	});
    };
}
)();
});
