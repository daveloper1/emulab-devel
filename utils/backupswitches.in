#!/usr/bin/perl
#
# Copyright (c) 2005-2024 University of Utah and the Flux Group.
# 
# {{{EMULAB-LICENSE
# 
# This file is part of the Emulab network testbed software.
# 
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This file is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
# License for more details.
# 
# You should have received a copy of the GNU Affero General Public License
# along with this file.  If not, see <http://www.gnu.org/licenses/>.
# 
# }}}
#

#
# WARNING: Somewhat Utah specific. We rely on certain users and config for
# this to work:
#
# force10: uses the (root alias) "toor" user which is expected to have root's
#          pubkey uploaded to allow password-less login.
# dellrest: just uses root ssh to admin user on switch, needs to have root
#           pubkey uploaded for admin user
# comware: just uses root ssh to switch, needs to have root pubkey encoded(?)
#          and uploaded (appears independent of user on switch).
#
my $BACKUPDIR = '@prefix@/backup/switch-config';
my $TMPFILE = "/tftpboot/switch-backup";
my $GRAB = "@prefix@/sbin/grabswitchconfig";

use lib '@prefix@/lib';
use libdb;
use lib '@prefix@/lib/snmpit';
use snmpit_lib;

use strict;
use English;

sub usage() {
    print "Usage: backupswitches <switches...>\n";
    exit 1;
}

if ($UID && !TBAdmin($UID)) {
    die "*** $0:\n" .
        "    Sorry, only admins get to run this script\n";
}

if (!@ARGV) {
    usage();
}

#
# Get today's date, nicely formatted
#
my ($junk,$junk,$junk,$mday,$mon,$year,$junk,$junk,$junk) = localtime(time);
$year += 1900;
$mon = sprintf("%02d",$mon + 1);
$mday = sprintf("%02d",$mday);
my $datestr = "$year-$mon-$mday";

#
# Back up each switch in turn
#
unlink $TMPFILE;
foreach my $switch (@ARGV) {
    #
    # Is it a Cisco or maybe an HP
    my $type = getDeviceType($switch);
    if ($type !~ /(arista|cisco|catalyst|hp|force10|dellrest|comware)/) {
	print STDERR
	    " *** $0:\n" .
	    "     ignoring unsupported switch type (type $type)\n";
	next;
    }
    if ($type =~ /(cisco|catalyst)/) {
	if (system("$GRAB $switch $TMPFILE")) {
	    print STDERR "Unable to grab config for $switch, ignored\n";
	    next;
	}
	if (system("mv $TMPFILE $BACKUPDIR/$switch-$datestr")) {
	    unlink($TMPFILE);
	    print STDERR "Unable to move config file to $BACKUPDIR/$switch-$datestr, ignored\n";
	    next;
	}
    }
    if ($type =~ /hp/) {
	if (system("scp $switch:/cfg/running-config $BACKUPDIR/$switch-$datestr")) {
	    print STDERR "Unable to grab and save config for $switch, ignored\n";
	    next;
	}
    }
    if ($type =~ /comware/) {
	#
	# XXX you must make sure the switch is backing up to startup.cfg.
	# Login to the switch and do "show startup" and make sure it is not
	# set to something like "flash:/sa-ch7.cfg". If it is, reset it with
	# "startup saved-configuration startup.cfg". This will change the
	# "Next main startup saved-configuration file". (Note that the
	# "Current startup saved-configuration file" will get changed on the
	# next reboot). After doing this, do a "save force" to backup the
	# current config to "startup.cfg".
	#
	if (system("scp $switch:startup.cfg $BACKUPDIR/$switch-$datestr")) {
	    print STDERR "Unable to grab and save config for $switch, ignored\n";
	    next;
	}
    }
    if ($type =~ /force10/) {
	my $args = "";
      again:
	if (system("ssh $args toor\@$switch write terminal 2>/tmp/$switch-$datestr.err > $BACKUPDIR/$switch.in")) {
	    my $err = $?;
	    my $errstr = `cat /tmp/$switch-$datestr.err`;
	    unlink("/tmp/$switch-$datestr.err");
	    if (($err >> 8) != 255) {
		print STDERR "----\n$errstr\n----";
		print STDERR "Unable to grab and save config for $switch ($?), ignored\n";
		next;
	    } elsif ($errstr =~ /Unable to negotiate/) {
		if ($args eq "") {
		    # try again once with old cipher/kexalgo
		    $args = "-c 3des-cbc ".
			    "-o KexAlgorithms=diffie-hellman-group1-sha1";
		    goto again;
		}
		print STDERR "----\n$errstr\n----";
		print STDERR "Negotiation error with $switch ($?), ignored\n";
		next;
	    } else {
		# XXX the switch can disconnect abruptly after doing its thing
		;
	    }
	}
        if (system("egrep -v \\#\\|Current\\|SupportAssist $BACKUPDIR/$switch.in \> $BACKUPDIR/$switch-$datestr")) {
	    unlink("$BACKUPDIR/$switch.in");
	    print STDERR "Unable to strip first and last lines for $switch, ignored\n";
	    next;
	}
	unlink("$BACKUPDIR/$switch.in");
    }
    if ($type =~ /dellrest/) {
	if (system("ssh admin\@$switch show running-config 2>/dev/null > $BACKUPDIR/$switch.in")) {
	    if (($? >> 8) != 255) {
		print STDERR "Unable to grab and save config for $switch ($?), ignored\n";
		next;
	    }
	}
	unlink("$BACKUPDIR/$switch-$datestr");
	rename("$BACKUPDIR/$switch.in", "$BACKUPDIR/$switch-$datestr");
    }
    if ($type =~ /arista/) {
	if (system("ssh toor\@$switch write terminal 2>/dev/null > $BACKUPDIR/$switch.in")) {
	    if (($? >> 8) != 255) {
		print STDERR "Unable to grab and save config for $switch ($?), ignored\n";
		next;
	    }
	}
	unlink("$BACKUPDIR/$switch-$datestr");
	rename("$BACKUPDIR/$switch.in", "$BACKUPDIR/$switch-$datestr");
    }
}

exit 0;
